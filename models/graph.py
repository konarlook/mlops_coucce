from collections import deque

import networkx as nx
import pandas as pd
from plotly import graph_objs as go

RECULC_PRESSURE = 101325
RECULC_Q = 86400
RECULC_TEMPERATURE = 273.15
STANDARD_RP = 25
STANDARD_MU = 10
STANDARD_WCT = 60
STANDARD_ROUGHNESS = 0.1


class PipelineNetwork(nx.DiGraph):
    def __init__(
            self,
            pipes_dataframe: pd.DataFrame,
            points_dataframe: pd.DataFrame,
            pvt_dataframe: pd.DataFrame,
    ):
        super(PipelineNetwork, self).__init__()
        self.pipes_dataframe = pipes_dataframe
        self.pvt_dataframe = pvt_dataframe
        self.points_dataframe = self.__merge_point_df(points_dataframe)
        self.root_point = self._search_root_point()
        self._build()

    def __merge_point_df(self, points_dataframe: pd.DataFrame):
        points_dataframe = points_dataframe.merge(
            self.pvt_dataframe, how="left", on="name"
        )  # TODO: on='id'
        return points_dataframe

    def _search_root_point(self) -> int:
        """
        Функция поиска узла подготовки нефти и газа (корень трубопроводной сети).
        :return: ID конечного узла трубопроводной системы.
        """
        root = int(
            self.points_dataframe[self.points_dataframe["obj_type"] == 6]["raw_id"]
        )
        return root

    def _build(self):
        __tamp_point = self.points_dataframe.rename(
            columns={"raw_id": "node_for_adding"}
        )
        for row in __tamp_point.to_dict(orient="records"):
            self.add_node(**row)
        __tamp_pipe = self.pipes_dataframe.rename(
            columns={"route_id_from": "v_of_edge", "route_id_to": "u_of_edge"}
        )
        for row in __tamp_pipe.to_dict(orient="records"):
            self.add_edge(**row)

    def draw(self):
        """
        Отрисовка координатного графика расположения скважин.
        :return: График pyplot с координатным расположением точек
        """

        def queue(a, b, qty: int) -> list:
            """
            Внутренняя функция определения середины отрезка.
            :param a: Начальная точка расчета (либо x0, либо y0);
            :param b: Конечная точка расчета (либо x1, либо y1);
            :param qty: Количество точек для создания окна информации на каждом отрезке трубопровода;
            :return: Список координат точек после разбиения.
            """
            q = deque()
            q.append((0, qty - 1))
            pts = [0] * qty
            pts[0] = a
            pts[-1] = b
            while len(q) != 0:
                left, right = q.popleft()
                center = (left + right + 1) // 2
                pts[center] = (pts[left] + pts[right]) / 2
                if right - left > 2:
                    q.append((left, center))
                    q.append((center, right))
            return pts

        def collector(
                x0: float, x1: float, y0: float, y1: float, qty: int, ht: str
        ) -> tuple[list, list, list]:
            """
            :param x0: Начальная координата отрезка по оси OX;
            :param x1: Конечная координата отрезка по оси OX;
            :param y0: Начальная координата отрезка по оси OY;
            :param y1: Конечная координата отрезка по оси OY;
            :param qty: Количество точек для создания окна информации на каждом отрезке трубопровода;
            :param ht: Информация об отрезке трубопровода;
            :return: Кортеж параметров точки (списко координат по оси OX, списко координат по оси OY, список
            строк информации об отрезке.
            """
            pth = [ht] * qty
            ptx = queue(x0, x1, qty + 2)
            pty = queue(y0, y1, qty + 2)
            ptx.pop(0)
            ptx.pop()
            pty.pop(0)
            pty.pop()
            return ptx, pty, pth

        edge_x, edge_y = [], []
        for edge in self.edges():
            print(self.edges[edge])
            x0, y0 = self.edges[edge]["begin_coord"]
            x1, y1 = self.edges[edge]["end_coord"]
            edge_x.append(x0)
            edge_x.append(x1)
            edge_x.append(None)
            edge_y.append(y0)
            edge_y.append(y1)
            edge_y.append(None)
        edge_trace = go.Scatter(
            x=edge_x,
            y=edge_y,
            line=dict(width=0.5, color="#888"),
            hoverinfo="text",
            textposition="bottom center",
            mode="lines",
        )
        m2x, m2y, m2t = [], [], []
        miss_node_x, miss_node_y = [], []
        for edge_fact in self.graph.edges(data=True):
            x0_miss, y0_miss = edge_fact[2]["begin_coord"]
            x1_miss, y1_miss = edge_fact[2]["end_coord"]
            miss_node_x.extend([x0_miss, x1_miss, None])
            miss_node_y.extend([y0_miss, y1_miss, None])
            hoverinfo = (
                    "ID: {}".format(edge_fact[2]["id"])
                    + "<br>Диаметр: {} см,</br>".format(edge_fact[2]["diameter"])
                    + "Длина: {:.2f} м,".format(edge_fact[2]["length"])
                    + "<br>Толщина стенки: {}</br>".format(edge_fact[2]["thickness"])
            )
            ptsx, ptsy, ptsh = collector(
                x0_miss, x1_miss, y0_miss, y1_miss, 8, hoverinfo
            )
            m2x.extend(ptsx)
            m2y.extend(ptsy)
            m2t.extend(ptsh)

        missing_node_trace = go.Scatter(
            x=m2x,
            y=m2y,
            showlegend=False,
            hovertemplate=m2t,
            name="Pipe",
            mode="markers",
            hoverinfo="none",
            marker=dict(size=0, reversescale=True, color="lightskyblue", opacity=0),
        )
        node_x, node_y = [], []
        for node in self.graph.nodes():
            x = self.graph.nodes[node]["coord_x"]
            y = self.graph.nodes[node]["coord_y"]
            node_x.append(x)
            node_y.append(y)
        node_adjacencies, node_q_text, node_name_text = [], [], []
        for node, adjacencies in enumerate(self.graph.adjacency()):
            node_adjacencies.append((len(adjacencies[1])))
            name = int(adjacencies[0])
            if self.graph.nodes[adjacencies[0]]["q_fluid"] is not None:
                q = self.graph.nodes[adjacencies[0]]["q_fluid"]
            else:
                q = 0
            node_q_text.append(
                "Id point: {}".format(name)
                + "<br>Дебит жидкости: {:.3f} м3/сут</br>".format(q)
                + "Имя: {}".format(self.graph.nodes[adjacencies[0]]["name"])
                + "<br>Давление на узле: {:.2f} атм</br>".format(
                    self.graph.nodes[adjacencies[0]]["p_out"] / RECULC_PRESSURE
                )
                + "<br>Газовый фактор: {:.3f} м3/м3</br>".format(
                    self.graph.nodes[adjacencies[0]]["rp"]
                )
                + "Удельная плотность газа: {:.3f}".format(
                    self.graph.nodes[adjacencies[0]]["gamma_gas"]
                )
                + "<br>Удельная плотность нефти: {:.3f}</br>".format(
                    self.graph.nodes[adjacencies[0]]["gamma_oil"]
                )
                + "Удельная плотность воды: {:.3f}".format(
                    self.graph.nodes[adjacencies[0]]["gamma_wat"]
                )
                + "<br>Давление насыщения: {:.3f} бар</br>".format(
                    self.graph.nodes[adjacencies[0]]["pb_atma"]
                )
                + "Обводненность: {:.3f} д.ед.".format(
                    self.graph.nodes[adjacencies[0]]["wct"]
                )
                + "<br>Газосодержание: {:.3f} м3/м3</br>".format(
                    self.graph.nodes[adjacencies[0]]["rsb"]
                )
                + "Вязкость нефти: {:.3f} мПа/с".format(
                    self.graph.nodes[adjacencies[0]]["muob_cp"]
                )
                + "<br>Объемный коэффициент: {:.3f}</br>".format(
                    self.graph.nodes[adjacencies[0]]["bob"]
                )
            )
            node_name_text.append(int(adjacencies[0]))
        node_trace = go.Scatter(
            x=node_x,
            y=node_y,
            mode="markers+text",
            hoverinfo="text",
            textposition="bottom center",
            name="Point",
            marker=dict(color="Red", size=5, line_width=0.5, reversescale=True),
            line=dict(color="#888", width=2),
        )
        node_trace.marker.size = node_adjacencies
        node_trace.text = node_name_text
        node_trace.hovertemplate = node_q_text
        fig = go.Figure(
            data=[edge_trace, node_trace, missing_node_trace],
            layout=go.Layout(
                title=dict(font=dict(size=8)),
                showlegend=False,
                hovermode="closest",
                xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            ),
        )
        fig.update_layout(font=dict(size=5, color="Black"))
        fig.show()


if __name__ == "__main__":
    from reader import *

    PATH = "../data/!param_without_luping.json"
    point = BaseNodeObject(source_type="JSON", json_path=PATH)
    pipe = BaseLineObject(source_type="JSON", json_path=PATH)
    pvt = BaseWellPVT(source_type="JSON", json_path=PATH)

    graph = PipelineNetwork(
        pipes_dataframe=pipe(), points_dataframe=point(), pvt_dataframe=pvt()
    )
    graph.draw()
